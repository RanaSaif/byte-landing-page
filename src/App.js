import ReactRoute from "./reactRoute/ReactRoute";

function App() {
  return (
    <div className="bg-[#F3EDF7] overflow-x-hidden relative">
      <div className="blob__pink effect__pink w-[240px] h-[240px]"></div>
      <div className="blob__blue effect__blue w-[240px] h-[240px] max-sm:hidden"></div>
      <ReactRoute />
    </div>
  );
}

export default App;