import React from 'react'
import Header from '../../components/homeComponents/header/Header';
import Trusted from '../../components/homeComponents/trusted/Trusted';
import Feature1 from '../../components/homeComponents/feature1/Feature1.jsx';
import Feature2 from '../../components/homeComponents/feature2/Feature2.jsx';

const Home = () => {
    return (
        <div className=''>

            <Header />

            <Trusted />

            <Feature1 />

            <Feature2 />

        </div>
    )
}

export default Home;