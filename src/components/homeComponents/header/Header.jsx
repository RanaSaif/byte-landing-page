import React, { useEffect } from 'react'

import Shery from "sheryjs";

const Header = () => {

  useEffect(() => {
    Shery.textAnimate(".textAnime", {
      style: 1,
      y: 10,
      delay: 0.1,
      duration: 1,
      ease: "cubic-bezier(0.23, 1, 0.320, 1)",
      multiplier: 0.1,
    });
  })

  return (
    <div className='min-h-[100vh] flex items-center gap-[24px] flex-col pt-[16vh] pb-[16vh] px-[64px] max-sm:px-[24px] z-[1 relative]'>

      <div className="flex items-center gap-[32px] relative">
        <div className='bg-gradient-to-b from-transparent to-[#F4F0F8] absolute inset-[-28px] z-[1]'></div>
        <img src="images/hero/file1.png" alt="" className='-rotate-[32deg] anime__left' />
        <img src="images/hero/file3.png" alt="" className='-translate-y-[36px] anime__center' />
        <img src="images/hero/file2.png" alt="" className='rotate-[32deg] anime__right' />
      </div>

      <span className='heading1 text-[var(--neutral-700)] max-w-[717px] text-center'>
        We provide <span className='text-[var(--primary-700)]'><span className='textAnime'>unlimited</span><br /> <span className='textAnime'>language</span></span> translation & unlimited <br /> <span className='text-[var(--primary-700)] textAnime'>projects</span>
      </span>

      <p className='text-[var(--neutral-700)] max-w-[1090px] text-center textAnimePara'>
        Lorem ipsum doloro sit it orem ipsum doloro sit it Lorem ipsum oro sit it Lorem ipsum doloro sit it orem  it Loem um oro sit it Lorem ipsum doloro sit it orem ipsum doloro sit it Lorem ipsum oro sit it Lorem  sit it orem ipsum doloro sit it Lorem ipsum oro sit it Lorem ipsum doloro sit it orem ipsum doloro sit it Lorem ipsum oro sit it Lorem ipsum doloro sit it orem ipsum doloro sit it Lorem ipsum oro sit it
      </p>

      <div className="flex items-center gap-[20px] max-sm:flex-col">

        <span className="btn">
          Get Started
        </span>

        <span className='text-[var(--primary-700)] right__to__left'>
          5000+ people used our website
        </span>

      </div>

    </div>
  )
}

export default Header