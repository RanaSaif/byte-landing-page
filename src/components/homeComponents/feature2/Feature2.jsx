import React, { useEffect, useRef } from 'react'
import Lottie from "lottie-react";

import { ScrollTrigger } from 'gsap/ScrollTrigger';
import InfinityLoader from '../../../InfinityLoader.json'
import gsap from 'gsap';
gsap.registerPlugin(ScrollTrigger)

const Feature2 = () => {

    const contentRef = useRef();
    const imageRef = useRef();

    useEffect(() => {
        const contentDiv = contentRef.current;
        const imageDiv = imageRef.current;
        gsap.from(contentDiv,
            {
                x: 300, duration: 1, opacity: '0', scrollTrigger: {
                    trigger: contentDiv,
                }
            },
        )
        gsap.from(imageDiv,
            {
                x: -300, duration: 1, opacity: '0', scrollTrigger: {
                    trigger: imageDiv,
                }
            },
        )
    })

    return (
        <div className='flex items-center justify-between px-[64px] pb-[24vh] gap-[32px] max-[1100px]:flex-col-reverse max-sm:px-[24px]  max-sm:gap-[16px]'>

            <div ref={imageRef} className="flex max-[1480px]:max-h-[560px] min-w-[480px] max-[1100px]:min-w-auto items-center justify-center effect__pink rounded-[26px] relative bg-[var(--primary-100)] overflow-hidden w-[710px] h-[610px] max-[1100px]:max-w-[600px] max-[1100px]:min-w-[730px] max-[1100px]:h-[692px] max-[860px]:min-w-[100%] max-[860px]:max-w-[100%]">
                <Lottie animationData={InfinityLoader} className='max-w-[320px] z-[1] max-[1100px]:max-w-[200px]' />
                <video src="images/feature2.mp4" autoPlay muted loop width={'100%'} height={'100%'} className='scale-[1.2] absolute h-full max-[1100px]:scale-[1.4] max-sm:scale-[1.5]'></video>
            </div>

            <div ref={contentRef} className="flex items-end flex-col gap-[24px] max-w-[694px] max-[1100px]:items-center">

                <h1 className="heading1 text-[var(--neutral-700)] text-end max-[1100px]:text-center">
                    A wide range of <span className='text-[var(--primary-700)]'>translation options</span> are <span>available</span>
                </h1>

                <p className="text-[var(--neutral-700)] text-end max-[1100px]:text-center">
                    Explore a myriad of translation choices at your fingertips. From document translations to interpreting services, our extensive offerings cater to diverse linguistic needs. Whether you seek precise technical translations or fluent localization for global audiences, our versatile solutions adapt to your requirements
                </p>

                <div className="flex items-center gap-[20px]">

                    <span className='text-[var(--primary-700)] left__to__right'>
                        5000+ people used our website
                    </span>

                    <span className="btn">
                        Login
                    </span>

                </div>

            </div>

        </div >
    )
}

export default Feature2;