import React from 'react'

const Trusted = () => {
  return (
    <div className="flex flex-col gap-[24px] w-full items-center pb-[24vh]">

      <span className='heading1 text-[var(--heading-700)]'>
        <span className='text-[var(--primary-700)]'>Trusted </span>by
      </span>

      <div className="flex items-center gap-[48px] justify-between w-full relative">

        <div className="absolute inset-0 z-[1] review__back"></div>

        <div className="flex items-center sliderAuto">

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/apple.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/huawei.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/meta.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/microsoft.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/samsung.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/tesla.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/google.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/amazon.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/apple.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/huawei.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/meta.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/microsoft.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/samsung.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/tesla.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/google.png" alt="" className='h-[28px]' />
          </div>

          <div className='flex items-center justify-center w-[240px]'>
            <img src="images/trusted/amazon.png" alt="" className='h-[28px]' />
          </div>

        </div>

      </div>

    </div>
  )
}

export default Trusted