import React, { useEffect, useRef } from 'react'

import { ScrollTrigger } from 'gsap/ScrollTrigger';
import gsap from 'gsap'
gsap.registerPlugin(ScrollTrigger)

const Feature1 = () => {

    const contentRef = useRef();
    const imageRef = useRef();

    useEffect(() => {
        const contentDiv = contentRef.current;
        const imageDiv = imageRef.current;
        gsap.from(contentDiv,
            {
                x: -300, duration: 1, opacity: 0, scrollTrigger: {
                    trigger: contentDiv,
                }
            },
        )
        gsap.from(imageDiv,
            {
                x: 300, duration: 1, opacity: 0, scrollTrigger: {
                    trigger: imageDiv,
                }
            },
        )
    })

    return (
        <div className='flex items-center justify-between px-[64px] pb-[24vh]  gap-[32px] max-[1100px]:flex-col max-sm:px-[24px] '>

            <div ref={contentRef} className="flex flex-col gap-[24px] max-w-[694px] max-[1100px]:items-center max-sm:gap-[16px]">

                <h1 className="heading1 text-[var(--neutral-700)] max-[1100px]:text-center">
                    A wide range of <span className='text-[var(--primary-700)]'>translation options</span> are <span>available</span>
                </h1>

                <p className="text-[var(--neutral-700)] max-[1100px]:text-center">
                    Unlock a world of linguistic possibilities with our extensive array of translation options. Whether you're looking to bridge language barriers in business communications, understand foreign literature, or connect with global audiences, our versatile suite of translation services has you covered
                </p>

                <div className="flex items-center gap-[20px]">

                    <span className="btn">
                        Login
                    </span>

                    <span className='text-[var(--primary-700)] right__to__left'>
                        5000+ people used our website
                    </span>

                </div>

            </div>

            <img ref={imageRef} src="images/feature1.png" alt="" className='effect__pink rounded-[26px] max-[1426px]:max-w-[600px] max-[1240px]:max-w-[500px] max-[1100px]:max-w-full' />

        </div>
    )
}

export default Feature1;