import React, { useEffect, useRef } from 'react'
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import gsap from 'gsap'

gsap.registerPlugin(ScrollTrigger)

function Footer() {

  const footerRef = useRef();

  useEffect(() => {
    const footerDiv = footerRef.current;
    gsap.from(footerRef,
      {
        y: -300, duration: 1, scrollTrigger: {
          trigger: footerDiv,
        }
      },
    )
  })

  return (
    <div className='m-[24px] p-[64px] bg-[var(--primary-100)] effect__pink rounded-[20px] max-sm:p-[16px]' ref={footerRef}>

      <div className="flex items-center w-full justify-between gap-[32px] max-[1100px]:flex-col">

        <div className="flex flex-col gap-[24px] max-w-[620px] max-xl:max-w-[540px] max-[1100px]:items-center">

          <img src="images/logo.png" className='w-[115px]' alt="" />

          <p className="text-[var(--neutral-700)] max-[1100px]:text-center">
            Byte brings you an innovative and user-friendly platform dedicated to simplifying the translation and localization process for your software applications and digital content. Whether you're a small development team or a large enterprise, Byte provides you with the tools needed to manage multiple languages efficiently and effectively.
          </p>

          <div className="flex items-center bg-[var(--primary-200)] relative gap-[10px] p-[10px] rounded-[20px] max-w-[436px] w-full max-[1100px]:max-w-full">

            <input type="text" className='bg-transparent z-[1] outline-none pl-[10px] inset-0 absolute w-full placeholder:text-[var(--primary-700)] text-[var(--primary-700)] pr-[140px]' placeholder='Enter your email' />

            <input type="text" className='w-full opacity-0 outline-none' />

            <div className="btn relative z-[1]">
              Submit
            </div>

          </div>

        </div>

        <div className="flex flex-col gap-[24px] max-[1100px]:items-center">

          <span className="heading3">
            General Links
          </span>

          <div className="flex flex-col gap-[16px]">

            <span className='px-[20px] py-[10px] bg-[var(--primary-200)] w-fit text-[var(--primary-700)] rounded-[12px]  cursor-pointer max-[1100px]:text-center'>
              Home
            </span>

            <span className='text-[var(--neutral-700)] cursor-pointer max-[1100px]:text-center'>
              About
            </span>

            <span className='text-[var(--neutral-700)] cursor-pointer max-[1100px]:text-center'>
              Services
            </span>

            <span className='text-[var(--neutral-700)] cursor-pointer max-[1100px]:text-center'>
              Contact
            </span>

          </div>

        </div>

        <div className="flex flex-col gap-[24px] max-[1100px]:items-center">

          <span className="heading3">
            More Links
          </span>

          <div className="flex flex-col gap-[16px]">

            <span className='text-[var(--neutral-700)] cursor-pointer max-[1100px]:text-center'>
              Terms & Conditions
            </span>

            <span className='text-[var(--neutral-700)] cursor-pointer max-[1100px]:text-center'>
              Cookie Policy
            </span>

            <span className='px-[20px] py-[10px] bg-[var(--primary-200)] w-fit  text-[var(--primary-700)] rounded-[12px] cursor-pointer max-[1100px]:text-center'>
              Privacy Policy
            </span>

            <span className='text-[var(--neutral-700)] cursor-pointer max-[1100px]:text-center'>
              Disclaimer
            </span>

          </div>

        </div>

      </div>

    </div>
  )
}

export default Footer