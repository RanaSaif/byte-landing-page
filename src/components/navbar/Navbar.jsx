import React from 'react'

function Navbar() {
    return (
        <div className="navbar__anime flex items-center justify-between z-[1] relative m-[24px] px-[24px] py-[15px] bg-[var(--primary-100)] rounded-[10px]">

            <div className="w-[181px]">
                <img src="images/logo.png" className="cursor-pointer" alt="" />
            </div>

            <div className="flex items-center">

                <span className="px-[32px] py-[14px] bg-[var(--primary-700)] text-[var(--primary-100)] rounded-[18px] cursor-pointer">
                    Home
                </span>

                <span className="px-[32px] py-[14px] text-[var(--primary-700)] cursor-pointer">
                    About
                </span>

                <span className="px-[32px] py-[14px] text-[var(--primary-700)] cursor-pointer">
                    Features
                </span>

                <span className="px-[32px] py-[14px] text-[var(--primary-700)] cursor-pointer">
                    Contact
                </span>

            </div>

            <div className="flex items-center gap-[20px]">

                <span className="text-[var(--primary-700)] cursor-pointer">
                    Sign Up
                </span>

                <span className="btn">
                    Login
                </span>

            </div>

        </div>
    )
}

export default Navbar;